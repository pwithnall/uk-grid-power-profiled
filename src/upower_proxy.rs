// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Endless OS Foundation, LLC

use zbus::{dbus_proxy, fdo};

#[dbus_proxy(
    interface = "org.freedesktop.UPower",
    default_service = "org.freedesktop.UPower",
    default_path = "/org/freedesktop/UPower"
)]
trait UPower {
    #[dbus_proxy(property)]
    fn on_battery(&self) -> fdo::Result<bool>;
}
