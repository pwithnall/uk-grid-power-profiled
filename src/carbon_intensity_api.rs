// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Endless OS Foundation, LLC

use chrono::{DateTime, Utc};
use rustify::{Client, Endpoint};
use rustify_derive::Endpoint;
use serde::Deserialize;
use serde_with::serde_as;

#[derive(Endpoint)]
#[endpoint(path = "intensity/{self.from.to_rfc3339()}/fw24h", method = "GET", response = "ForecastResponse")]
struct ForecastEndpoint {
    #[endpoint(skip)]
    pub from: DateTime<Utc>,
}

#[derive(Deserialize, Debug)]
struct ForecastResponse {
    pub data: Vec<ForecastPeriod>,
}

#[serde_as]
#[derive(Deserialize, Debug)]
pub struct ForecastPeriod {
    #[serde(with = "iso8601_without_seconds")]
    pub from: DateTime<Utc>,
    #[serde(with = "iso8601_without_seconds")]
    pub to: DateTime<Utc>,
    pub intensity: ForecastIntensity,
}

#[derive(Deserialize, Debug)]
pub struct ForecastIntensity {
    pub forecast: u16,
    pub actual: Option<u16>,
    pub index: String,
}

pub struct Api {
    client: Client,
}

impl Api {
    pub fn new_default() -> Api {
        Api { client: Client::default("https://api.carbonintensity.org.uk") }
    }

    pub async fn forecast_24h(self: &Api, from: DateTime<Utc>) -> Result<Vec<ForecastPeriod>, rustify::errors::ClientError> {
        let endpoint = ForecastEndpoint { from: from };
        let result = endpoint.exec(&self.client).await;
        return Ok(result?.parse()?.data);
    }
}

// We have to use a custom parser because the one in chrono (which is the `%+` format string)
// doesn’t accept ISO 8601 dates which have the `:ss` seconds missing, even though they’re fully
// valid.
//
// See https://github.com/chronotope/chrono/issues/330
// and https://github.com/chronotope/chrono/issues/244
mod iso8601_without_seconds {
    use chrono::{DateTime, Utc, TimeZone};
    use serde::{self, Deserialize, Deserializer};

    const FORMAT: &str = "%FT%RZ";

    pub fn deserialize<'de, D>(deserializer: D) -> Result<DateTime<Utc>, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        Utc
            .datetime_from_str(&s, FORMAT)
            .map_err(serde::de::Error::custom)
    }
}
