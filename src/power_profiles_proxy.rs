// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Endless OS Foundation, LLC

use zbus::{Result, dbus_proxy};

#[dbus_proxy(
    interface = "net.hadess.PowerProfiles",
    default_service = "net.hadess.PowerProfiles",
    default_path = "/net/hadess/PowerProfiles"
)]
trait PowerProfiles {
    async fn hold_profile(&self, profile: &str, reason: &str, application_id: &str) -> Result<u32>;
    async fn release_profile(&self, cookie: u32) -> Result<()>;
}
