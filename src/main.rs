// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright 2023 Endless OS Foundation, LLC

use chrono::{Timelike, Utc};
use tokio;
use zbus::Connection;
//use futures_util::stream::StreamExt;

mod carbon_intensity_api;
mod power_profiles_proxy;
mod upower_proxy;

const APPLICATION_ID: &str = "com.gitlab.pwithnall.UkGridPowerProfiled";

#[tokio::main]
async fn main() {
    //loop {
    //    read_current_intensity
    //    read_current_plugged_in_status
    //    decide_power_profile
    //    set_power_profile
    //    sleep
    //}

    let now = Utc::now();
    let forecast_client = carbon_intensity_api::Api::new_default();
    let dbus_connection = Connection::system().await.unwrap();
    let power_profiles_proxy = power_profiles_proxy::PowerProfilesProxy::new(&dbus_connection).await.unwrap();
    let upower_proxy = upower_proxy::UPowerProxy::new(&dbus_connection).await.unwrap();
    let mut cookie = 0;

    loop {
        // TODO: Cache this
        let forecast = forecast_client.forecast_24h(now).await.unwrap();
        println!("forecast: {:?}", forecast);

        // Find the forecast period which applies now
        let now_period = forecast.iter().find(|x| x.from <= now && x.to > now).unwrap();
        println!("now period: {:?}", now_period);

        let on_battery = upower_proxy.on_battery().await.unwrap();
        println!("on battery: {on_battery}");

        if now_period.intensity.forecast >= 200 && !on_battery {
            // TODO: Profile D-Bus usage
            // TODO: Connect to signals
            // TODO: Drop .unwrap() calls
            // TODO: Format code
            let reply = power_profiles_proxy.hold_profile("power-saver", "National Grid carbon intensity is too high", APPLICATION_ID).await.unwrap();
            println!("hold: {reply}");
            cookie = reply;
        } else if cookie != 0 {
            let reply = power_profiles_proxy.release_profile(cookie).await.unwrap();
            println!("release done");
        }

        // Schedule the next check at 2 minutes past the next :00 or :30 time
        let mut span = 0;
        if now.time().minute() % 30 != 0 {
            span = 30 - (now.time().minute() % 30);
        }
        span += 2;

        let start = tokio::time::Instant::now() + chrono::Duration::minutes(span.into()).to_std().unwrap();
        let period = chrono::Duration::minutes(30).to_std().unwrap();

        println!("Waiting starts at {start:?}, period {period:?}");
        let mut interval = tokio::time::interval_at(start, period);

        interval.tick().await;
    }






    // TODO let mut upower_property_changed_stream = upower_proxy.receive_property_changed::<bool>("OnBattery").await;
    //let signal = upower_property_changed_stream.next().await.unwrap();
    //println!("signal: {:?}", signal.name());

    ()
}
