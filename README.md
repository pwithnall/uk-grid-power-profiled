# uk-grid-power-profiled

A small daemon which periodically checks the UK National Grid carbon intensity
and sets
[power-profiles-daemon](https://gitlab.freedesktop.org/hadess/power-profiles-daemon/)
to power saving mode if the carbon intensity is high.

This is mostly written as a way for me to learn Rust, but it could eventually
have a real use if tidied up and distributed.
